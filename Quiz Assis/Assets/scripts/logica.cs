﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class Quiz
{
    public string texto;
}


public class ClassRespostas : Quiz
{//INICIO CLASS RESPOSTAS
    public bool ecorreta;

    public ClassRespostas(string a, bool b)
    {
        this.texto = a;
        this.ecorreta = b;
    }
}//FIM CLASS RESPOSTAS




public class ClassPergunta : Quiz
{// INICIO CLASS PERGUNTAS 

    public int pontos;
    public ClassRespostas[] respostas;


    //METODO PARA CRIAR AS PERGUNTAS
    public ClassPergunta(string t, int p)
    {
        this.texto = t;
        this.pontos = p;
    }

    //METODO PARA ADD AS RESPOSTAS
    public void AddResposta(string a, bool b)
    {
        if (respostas == null)
        {
            respostas = new ClassRespostas[4];
        }

        for (int i = 0; i < respostas.Length; i++)
        {
            if (respostas[i] == null)
            {
                respostas[i] = new ClassRespostas(a, b);
                break;
            }
        }
    }
    // LISTAR OU EXIBIR AS RESPOSTAS

    public void ListarRespostas()
    {
        if (respostas != null && respostas.Length > 0)
        {
            foreach (ClassRespostas r in respostas)
            {
                Debug.Log(r);
            }
        }
    }

    //CHECAR RESPOSTA CORRETA
    public bool ChecarRespostaCorreta(int pos)
    {
        if (respostas != null && respostas.Length > pos && pos >= 0)
        {
            return respostas[pos].ecorreta;
        }
        else
        {
            return false;
        }
    }

}// FIM CLASS PERGUNTAS 


public class logica : MonoBehaviour
{//Inicio Class Logica

    public Text TituloPergunta;
    public Button[] respostaBtn = new Button[3];
    public List<ClassPergunta> PerguntasLista = new List<ClassPergunta>();

    private ClassPergunta perguntaAtual;


    public GameObject PainelCorreto;
    public GameObject PainelErrado;
    public GameObject PainelFinal;
    private int contagemPerguntas = 0;
    private float tempo;
    private bool carregarProximaPergunta;
    private int pontos = 0;


    void Start()
    {//Inicio perguntas 
        ClassPergunta p1 = new ClassPergunta("1.  Qual era a cor da sala quando a Ana Paula caiu da escada?", 5);
        p1.AddResposta("A - Azul", false);
        p1.AddResposta("B - Verde", false);
        p1.AddResposta("C - Verde água", true);
        p1.AddResposta("D - Verde Tiffany", false);

        ClassPergunta p2 = new ClassPergunta("2.  Em uma das primeiras aulas “particulares” de direção, a Luana derrubou o carro de um lugar. Onde?", 10);
        p2.AddResposta("A - Um barranco", false);
        p2.AddResposta("B - Um rio", true);
        p2.AddResposta("C - Nas pedreiras", false);
        p2.AddResposta("D - Na ponte", false);

        ClassPergunta p3 = new ClassPergunta("3.  Quando a Alzenira estava aprendendo a dirigir, ela quase matou os bichos do sítio do avô. Quais eram os bichos?", 3);
        p3.AddResposta("A - Porcos", false);
        p3.AddResposta("B - Pássaros", false);
        p3.AddResposta("C - Galinhas", true);
        p3.AddResposta("D - Cachorros", false);

        ClassPergunta p4 = new ClassPergunta("4.  Com quantos anos a Luana bateu em um cara na rua com uma bicicleta?", 4);
        p4.AddResposta("A - 16", false);
        p4.AddResposta("B - 17", true);
        p4.AddResposta("C - 18", false);
        p4.AddResposta("D - 19", false);

        ClassPergunta p5 = new ClassPergunta("5.  Quando o Paulo Henrique era menor, um eletrodoméstico já caiu nele. Qual produto era esse?", 5);
        p5.AddResposta("A - Geladeira", false);
        p5.AddResposta("B - Fogão", false);
        p5.AddResposta("C - TV", true);
        p5.AddResposta("D - Microondas", false);

        ClassPergunta p6 = new ClassPergunta("6.  Tinhamos um VHS na sala, porém ele parou de funcionar porque o Paulo Henrique:", 8);
        p6.AddResposta("A - Enfiou uma fita vazia", false);
        p6.AddResposta("B - Jogou no chão", false);
        p6.AddResposta("C - Encheu de arroz cru", true);
        p6.AddResposta("D - Colocou um lápis", false);

        ClassPergunta p7 = new ClassPergunta("7.  A Ana Paula tem o dente da frente quebrado porque o Paulo Henrique jogou:", 5);
        p7.AddResposta("A - Uma bola de Futebol", false);
        p7.AddResposta("B - Um pião", false);
        p7.AddResposta("C - Um tênis", false);
        p7.AddResposta("D - Uma bolinha de gude", true);

        ClassPergunta p8 = new ClassPergunta("8.  Qual era a música que estava em alta no chá de bebê da Ana Clara?", 20);
        p8.AddResposta("A - Boom Boom Pow - The Black Eyes Peas", true);
        p8.AddResposta("B - Drop It Like It’s Hot - Snoop Dogg", false);
        p8.AddResposta("C - Crazy In Love - Beyonce feat Jay-z", false);
        p8.AddResposta("D - I´m Glad - Jennifer Lopez", false);

        ClassPergunta p9 = new ClassPergunta("9.  Quem não gostava de receber presente em dia de aniversário?", 5);
        p9.AddResposta("A - Luana", false);
        p9.AddResposta("B - Juliana", false);
        p9.AddResposta("C - Ana Clara", false);
        p9.AddResposta("D - Ana Paula", true);

        ClassPergunta p10 = new ClassPergunta("10.  Quem bateu na cara de um garoto dentro do ônibus?", 3);
        p10.AddResposta("A - Alzenira", true);
        p10.AddResposta("B - Juliana", false);
        p10.AddResposta("C - Ana Clara", false);
        p10.AddResposta("D - Luana", false);


        PerguntasLista.Add(p1);
        PerguntasLista.Add(p2);
        PerguntasLista.Add(p3);
        PerguntasLista.Add(p4);
        PerguntasLista.Add(p5);
        PerguntasLista.Add(p6);
        PerguntasLista.Add(p7);
        PerguntasLista.Add(p8);
        PerguntasLista.Add(p9);
        PerguntasLista.Add(p10);

        perguntaAtual = p1;

        ExibirPerguntasNoQuiz();
    }//Fim perguntas

    // Inicio Perguntas
    void Update()
    {
        if (carregarProximaPergunta == true)
        {
            tempo += Time.deltaTime;
            if (tempo > 3)
            {
                tempo = 0;
                carregarProximaPergunta = false;
                ProximaPergunta();
            }
        }
    }



    private void ExibirPerguntasNoQuiz()
    {
        TituloPergunta.text = perguntaAtual.texto;
        respostaBtn[0].GetComponentInChildren<Text>().text = perguntaAtual.respostas[0].texto;
        respostaBtn[1].GetComponentInChildren<Text>().text = perguntaAtual.respostas[1].texto;
        respostaBtn[2].GetComponentInChildren<Text>().text = perguntaAtual.respostas[2].texto;
        respostaBtn[3].GetComponentInChildren<Text>().text = perguntaAtual.respostas[3].texto;
    }

    public void ProximaPergunta()
    {
        contagemPerguntas++;
        if (contagemPerguntas < PerguntasLista.Count)
        {
            perguntaAtual = PerguntasLista[contagemPerguntas];
            ExibirPerguntasNoQuiz();
            PainelCorreto.SetActive(false);
            PainelErrado.SetActive(false);
        }
        else
        {
            PainelCorreto.SetActive(false);
            PainelErrado.SetActive(false);
            PainelFinal.SetActive(true);
            PainelFinal.transform.GetComponentInChildren<Text>().text = "Pontos: " + pontos.ToString();
        }
    }


    public void ClickResposta(int pos)
    {
        if (perguntaAtual.respostas[pos].ecorreta)
        {
            PainelCorreto.SetActive(true);
            pontos += perguntaAtual.pontos;
        }
        else
        {
            PainelErrado.SetActive(true);
        }

        carregarProximaPergunta = true;
    }

    public void IniciarQuiz()
    {
        SceneManager.LoadScene("Quiz");
    }

    public void MenuInicial()
    {
        SceneManager.LoadScene("MenuInicial");
    }

    public void MenuFinal()
    {
        SceneManager.LoadScene("MenuFinal");
    }

    public void Credito()
    {
        SceneManager.LoadScene("Credito");
    }

    public void SairJogo()
    {
        Application.Quit();
    }

    public void ComoJogar()
    {
        SceneManager.LoadScene("ComoJogar");
    }
}//fim class